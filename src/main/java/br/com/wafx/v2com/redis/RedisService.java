package br.com.wafx.v2com.redis;

import javax.enterprise.context.ApplicationScoped;

import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.RedisConnection;
import com.lambdaworks.redis.RedisURI;

@ApplicationScoped
public class RedisService {
	
	private RedisClient redisClient;
	private RedisConnection<String, String> connection;
	
	public String getValue(String key) {
		return connect().get(key);
	}
	
	public String setValue(String key, String value) {
		return connect().set(key, value);
	}
	
	public RedisConnection<String, String> connect() {
		redisClient = createRedisClient();
		if (connection == null)
			connection = redisClient.connect();
		return connection;
	}

	private RedisClient createRedisClient() {
		if (redisClient == null)
			return new RedisClient(RedisURI.create("redis://localhost:6379"));
		return redisClient;
	}
	
	public void shutdown() {
		connection.close();
		redisClient.shutdown();
	}
	
}