package br.com.wafx.v2com.restClient;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

/**
 * @author Kaio Maximiano
 */

@Path("/sap")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@RegisterRestClient
public interface RestClientMedidor {
		
	@GET
	@Path("/listaMedidorCorte")
	String findListaMedidor();
}
