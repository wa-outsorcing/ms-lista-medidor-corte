package br.com.wafx.v2com.model;

import java.util.Date;

public class Medidor {

	private String noteNumber; // Número da Nota de suspensão de energia
	private String meterId;// Código de identificação do medidor 
	private String installationCode; //Código da instalação	
	private Date noteDate; // Data da nota
	private Integer openInvoicesNumber; // Número de faturas em aberto
	private Boolean supplyGuarantee; // Garantia de fornecimento
	private Integer cityId; // Cidade 
	private Integer stateId; // Estado
	private Date onSiteBillingDate; // Data da leitura do faturamento (on site billing)
	private Boolean holiday; // Deve considerar o dia atual como feriado para esta UC
	
	public String getNoteNumber() {
		return noteNumber;
	}

	public void setNoteNumber(String noteNumber) {
		this.noteNumber = noteNumber;
	}

	public String getMeterId() {
		return meterId;
	}

	public void setMeterId(String meterId) {
		this.meterId = meterId;
	}

	public String getInstallationCode() {
		return installationCode;
	}

	public void setInstallationCode(String installationCode) {
		this.installationCode = installationCode;
	}

	public java.util.Date getNoteDate() {
		return noteDate;
	}

	public void setNoteDate(java.util.Date noteDate) {
		this.noteDate = noteDate;
	}

	public Integer getOpenInvoicesNumber() {
		return openInvoicesNumber;
	}

	public void setOpenInvoicesNumber(Integer openInvoicesNumber) {
		this.openInvoicesNumber = openInvoicesNumber;
	}

	public Boolean getSupplyGuarantee() {
		return supplyGuarantee;
	}

	public void setSupplyGuarantee(Boolean supplyGuarantee) {
		this.supplyGuarantee = supplyGuarantee;
	}

	public Integer getCityId() {
		return cityId;
	}

	public void setCityId(Integer cityId) {
		this.cityId = cityId;
	}

	public Date getOnSiteBillingDate() {
		return onSiteBillingDate;
	}

	public void setOnSiteBillingDate(Date onSiteBillingDate) {
		this.onSiteBillingDate = onSiteBillingDate;
	}

	public Boolean getHoliday() {
		return holiday;
	}

	public void setHoliday(Boolean holiday) {
		this.holiday = holiday;
	}

	public Integer getStateId() {
		return stateId;
	}

	public void setStateId(Integer stateId) {
		this.stateId = stateId;
	}

	@Override
	public String toString() {
		return "Medidor [noteNumber=" + noteNumber + ", meterId=" + meterId + ", installationCode=" + installationCode
				+ ", noteDate=" + noteDate + ", openInvoicesNumber=" + openInvoicesNumber + ", supplyGuarantee="
				+ supplyGuarantee + ", cityId=" + cityId + ", stateId=" + stateId + ", onSiteBillingDate="
				+ onSiteBillingDate + ", holiday=" + holiday + "]";
	}
	
	
	
}
