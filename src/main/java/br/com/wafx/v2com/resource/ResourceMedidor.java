package br.com.wafx.v2com.resource;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.eclipse.microprofile.openapi.annotations.Operation;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import br.com.wafx.v2com.redis.RedisService;
import br.com.wafx.v2com.restClient.RestClientMedidor;

/**
 * @author Kaio Maximiano
 */
@Path("/v2com")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ResourceMedidor {

	@Inject
	@RestClient
	RestClientMedidor restClientMdmVision;
	
	@Inject
	RedisService redisService; 

	@GET
	@Path("/listaMedidorCorte")
	@Operation(description = "Endpoint REST para Busca de Medidores para corte", summary = "Endpoint REST para Busca de Medidores para corte")
	public Response findListaMedidor() {
		String result = restClientMdmVision.findListaMedidor();
		if (result == null || result.isEmpty())
			return Response.status(Status.NOT_FOUND.ordinal()).build();
		
		//TODO: DEPOIS SALVA A LISTA NO REDIS COM A CHAVE note_pending:$meterId
		return Response.ok(result).build();
	}
}